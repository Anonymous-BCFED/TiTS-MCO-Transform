import os, sys, re
from buildtools import log, os_utils
TITS_DIR=os.path.join('F:\\', 'Projects', 'TiTS-MCO-Patcher', 'lib', 'TiTS-Public')

# --- "before\\appearance.as"	2019-05-05 13:55:40.872689900 -0700
# +++ "after\\appearance.as"	2019-05-05 13:55:42.085655200 -0700
RE_DATE_FIX = re.compile(rb'^([\-\+]{3})(\s+)("[^"]+")\s+\d{4}\-\d{2}\-\d{2} \d{2}:\d{2}:[0-9\.]+ \-?\d+$')
assert RE_DATE_FIX.match(b'--- "before\\appearance.as"	2019-05-08 17:05:22.257832300 -0700') is not None
units={
    'perks':{
        'Lupinol': os.path.join(TITS_DIR, 'classes', 'Items', 'Transformatives', 'Lupinol.as')
    },
    'chars': {
        'ZilMale': os.path.join(TITS_DIR, 'classes', 'Characters', 'ZilMale.as'),
        'AraKei': os.path.join(TITS_DIR, 'classes', 'Characters', 'AraKei.as'), # Had issues with SuperStatementNode.
        #'Kara': os.path.join(TITS_DIR, 'classes', 'Characters', 'Kara.as'), # Had issues with SuperStatementNode.
        #'Anno': os.path.join(TITS_DIR, 'classes', 'Characters', 'Anno.as'), # Had issues with new.
    },
    'includes': {
        #'debug': os.path.join(TITS_DIR, 'includes', 'debug.as'), # CDATA
        'appearance': os.path.join(TITS_DIR, 'includes', 'appearance.as'),
        'mhenga-rooms': os.path.join(TITS_DIR, 'includes', 'mhenga', 'rooms.as'),
        #'myrna': os.path.join(TITS_DIR, 'includes', 'holidayEvents', 'myrnaTheGenerousKorgonne.as'),
        #'creation': os.path.join(TITS_DIR, 'includes', 'creation.as'),
        #'varmint_wrangling': os.path.join(TITS_DIR, 'includes', 'newTexas', 'varmint_wrangling.as'),
        #'creation_custom_PCs': os.path.join(TITS_DIR, 'after', 'includes', 'creation_custom_PCs.as'),
    },
    'classes': {
        'TiTS': os.path.join(TITS_DIR, 'classes', 'TiTS.as'), #includes
        #'ParseEngine': os.path.join(TITS_DIR, 'classes', 'Parser', 'ParseEngine.as'), #PerkData error
        'Creature': os.path.join(TITS_DIR, 'classes', 'Creature.as'), # var hurr = hasPerk("")
        'MainButton': os.path.join(TITS_DIR, 'classes', 'UIComponents', 'MainButton.as'), # Issues with `new`
        #'CustomBust': os.path.join(TITS_DIR, 'classes', 'Resources', 'CustomBust.as'), # try/catch
        #'WeaponModule': os.path.join(TITS_DIR, 'classes', 'Ships', 'Modules', 'UpgradeModules', 'WeaponModule.as'),
        'RoomClass': os.path.join(TITS_DIR, 'classes', 'RoomClass.as'),
        'UIStyleSettings': os.path.join(TITS_DIR, 'classes', 'UIComponents', 'UIStyleSettings.as'),
    },
    'items': {
        'UthraPlus': os.path.join(TITS_DIR, 'classes','Items','Transformatives','UthraPlus.as')
    },
    'functions': {
        'num2Ordinal': os.path.join(TITS_DIR, 'classes', 'Engine', 'Utility', 'num2Ordinal.as') # const outside of package
    }
}

os_utils.ensureDirExists('examples', noisy=True)
os_utils.cmd([
    'java',
    '-jar',os.path.join('dist', 'tits-mco-transform.jar'),
    '--skip-processing',
    '--dump-nsconfig', os.path.join('examples', 'tits.nsconfig.yml'),
    os.path.join(TITS_DIR, 'classes', 'TiTS.as')],
    show_output=True,
    echo=True,
    globbify=False)

for unit, cases in units.items():
    for casename, casefile in cases.items():
        outfile = os.path.join('examples', unit, 'before', casename)
        os_utils.ensureDirExists(os.path.dirname(outfile), noisy=True)
        os_utils.cmd([
            'java',
            '-jar',os.path.join('dist', 'tits-mco-transform.jar'),
            '--skip-processing',
            '--as-xml', outfile+'.xml',
            '--as-coffee', outfile+'.coffee',
            '--as-as3', outfile+'.as',
            casefile],
            show_output=True,
            echo=True,
            globbify=False)

        outfile = os.path.join('examples', unit, 'after', casename)
        os_utils.ensureDirExists(os.path.dirname(outfile), noisy=True)
        os_utils.cmd([
            'java',
            '-jar',os.path.join('dist', 'tits-mco-transform.jar'),
            '--as-xml', outfile+'.xml',
            '--as-coffee', outfile+'.coffee',
            '--as-as3', outfile+'.as',
            casefile],
            show_output=True,
            echo=True,
            globbify=False)

        with os_utils.Chdir(os.path.join('examples', unit), quiet=False):
            for ext in ['xml', 'as', 'coffee']:
                stdout, stderr = os_utils.cmd_output([
                    'diff',
                    '-u',
                    os.path.join('before', casename+'.'+ext),
                    os.path.join('after', casename+'.'+ext),
                ], echo=True)
                with open('.'.join([casename, ext, 'diff']), 'wb') as f:
                    for line in stdout.splitlines():
                        line = RE_DATE_FIX.sub(b'\\1\\2\\3', line)
                        f.write(line+b'\n')
