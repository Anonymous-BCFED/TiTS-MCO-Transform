from buildtools import os_utils
from urllib.parse import urlparse
import re

#INCLUDE_PACKAGES=[
#    'chardet',
#    'ftfy',
#    'pybuildtools',
#    'lxml',
#    'patch',
#    'pygit2',
#    'PyYAML',
#    'semantic-version'
#]

# -e git+git@gitlab.com:N3X15/python-build-tools.git@1c7ceb15e52dd283dad171e44b88cba51f8fd5f2#egg=pybuildtools
REG_EDITABLE_GIT_SSH=re.compile(r'git\+(?P<user>[a-z]+)@(?P<netloc>[a-zA-Z0-9\.\-]+(:\d+)?):(?P<path>[^@]+)@(?P<commit>[a-f0-9]+)#egg=(?P<package>[a-zA-Z0-9\-]+)')
# -e git+https://gitlab.com/N3X15/python-build-tools.git@1c7ceb15e52dd283dad171e44b88cba51f8fd5f2#egg=pybuildtools
REG_EDITABLE_GIT_HTTP=re.compile(r'git\+https?://(?P<netloc>[a-zA-Z0-9\.\-]+(:\d+)?)(?P<path>/[^@]+)@(?P<commit>[a-f0-9]+)#egg=(?P<package>[a-zA-Z0-9\-]+)')

def fix_ssh_match(m):
    netloc=m.group('netloc')
    path=m.group('path')
    commit=m.group('commit')
    package=m.group('package')

    return f'git+https://{netloc}/{path}@{commit}#egg={package}'

rawLinesToInclude=[]
required=[]
with open('requirements.txt', 'r') as f:
    for line in f:
        line=line.strip()
        if line.startswith('#'):
            continue
        if line.startswith('-e ') and 'site-packages' in line:
            continue
        if REG_EDITABLE_GIT_SSH.search(line):
            print(line)
            rawLinesToInclude += [REG_EDITABLE_GIT_SSH.sub(fix_ssh_match, line)]
            continue
        if REG_EDITABLE_GIT_HTTP.search(line):
            print(line)
            rawLinesToInclude += [line]
            continue

        packageID=''
        if '#egg=' in line:
            packageID = line.split('#')[-1][4:]
        else:
            packageID = line.split('=')[0]
        #print(repr(packageID))
        required+=[packageID]

stdout, stderr = os_utils.cmd_output([os_utils.which('pip3.7'), 'freeze', '--no-color', '--disable-pip-version-check'], echo=False, critical=True, globbify=False)

newlines=[]
for line in str(stdout.decode('utf-8')+stderr.decode('utf-8')).splitlines():
    line=line.strip()
    if line.startswith('WARNING:'):
        print('[pip] '+line)
        continue
    if line.startswith('# Editable install with no version control'):
        continue
    if line.startswith('-e ') and 'site-packages' in line:
        continue
    if REG_EDITABLE_GIT_SSH.search(line):
        #line = REG_EDITABLE_GIT_SSH.sub(fix_ssh_match, line)
        continue

    packageID=''
    if '#egg=' in line:
        packageID = line.split('#')[-1][4:]
    else:
        packageID = line.split('=')[0]
    #print(repr(packageID))
    if packageID in required:
        newlines+=[line]
len_nl = len(newlines)
len_req = len(required)
if len_nl < len_req:
    print(f'Missing {len_req-len_nl} packages in newlines')
newlines += rawLinesToInclude
newlines.sort()
with open('requirements.new.txt', 'w') as f:
    for e in newlines:
        f.write(f'{e}\n')
