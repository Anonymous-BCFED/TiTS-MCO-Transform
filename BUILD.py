#pylint: disable=C0301
import os
import sys

import lxml

from buildtools import ENV, Chdir, cmd, log, os_utils
from buildtools.config import YAMLConfig

PACKAGE_NAME='tits-mco-transform'
PACKAGE_VERSION='0.0.1-SNAPSHOT'

SCRIPT_DIR = os.path.abspath(os.path.dirname(__file__))
JAVA_ROOT = os.path.dirname(SCRIPT_DIR)
MAVENIZER_HOME = os.path.join(JAVA_ROOT, 'flex-utilities', 'flex-maven-tools', 'flex-sdk-converter')
# F:\Projects\OpenElysium\java\flex-utilities\flex-maven-tools\flex-sdk-converter\cli\target\apache-flex-sdk-converter-1.0.0-SNAPSHOT.jar
MAVENIZER_JAR = os.path.join(MAVENIZER_HOME, 'cli', 'target', 'apache-flex-sdk-converter-1.0.0-SNAPSHOT.jar')
MAVENIZER_VERSION = '4.17.0'

config = YAMLConfig('config.yml', {
    'path': {
        'flex-sdk': 'C:\\My\\Flex\\SDK',
        'maven-repo': 'C:\\Maven-Repo',
        'deploy-to': []
    }
})
if ENV.get('JDK_HOME') is not None:
    ENV.set('JAVA_HOME', ENV.get('JDK_HOME'))
needed = os.path.join(config.get('path.maven-repo'), 'org', 'apache', 'flex', 'compiler', 'swfutils', MAVENIZER_VERSION, 'swfutils-{}.jar'.format(MAVENIZER_VERSION))
if '--remavenize' in sys.argv or not os.path.isfile(needed):
    with Chdir(MAVENIZER_HOME):
        if not os.path.isfile(MAVENIZER_JAR):
            cmd(['mvn', 'package'], critical=True, echo=True, show_output=False)
        cmd(['java', '-jar', MAVENIZER_JAR, 'convert', '-fdkDir', config.get('path.flex-sdk'), '-mavenDir', config.get('path.maven-repo')], critical=True, echo=True, show_output=True)
with Chdir(SCRIPT_DIR):
    os_utils.safe_rmtree('dist')
    cmd(['mvn', 'clean', 'package'], critical=True, echo=True, show_output=True)
    os_utils.ensureDirExists('dist')
    os_utils.single_copy(os.path.join('target', PACKAGE_NAME+'-'+PACKAGE_VERSION+'.jar'), os.path.join('dist', PACKAGE_NAME+'.jar'), verbose=True)
    os_utils.copytree(os.path.join('target', 'lib'), os.path.join('dist', 'lib'), verbose=True)

    path_to_jar=os.path.abspath(os.path.join('dist', PACKAGE_NAME+'.jar'))
    with log.info('Writing dist/tits-mco-transform.sh...'):
        with open('dist/tits-mco-transform.sh', 'w') as f:
            f.write('#!/bin/bash\n')
            f.write('SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"\n')
            f.write(f'java -jar $SCRIPTPATH/{PACKAGE_NAME}.jar $*\n')
    with log.info('Writing dist/tits-mco-transform.cmd...'):
        with open('dist/tits-mco-transform.cmd', 'w') as f:
            f.write('@echo off\n')
            f.write(f'java -jar %~dp0{PACKAGE_NAME}.jar %*\n')

    distpaths = os_utils.get_file_list('dist')
    for deploy_dir in config.get('path.deploy-to', []):
        with log.info('Copying to %s...', deploy_dir):
            targetpaths=[]
            if os.path.isdir(deploy_dir):
                targetpaths = os_utils.get_file_list(deploy_dir)
            for targetpath in targetpaths:
                if targetpath.endswith('README.md'):
                    continue
                fullpath = os.path.join(deploy_dir, targetpath)
                if targetpath not in distpaths:
                    log.info('RM %s', targetpath)
                    os.remove(fullpath)
            for srcpath in distpaths:
                fullpath = os.path.join(deploy_dir, srcpath)
                os_utils.single_copy(os.path.join('dist', srcpath), os.path.join(deploy_dir, srcpath), verbose=True)
