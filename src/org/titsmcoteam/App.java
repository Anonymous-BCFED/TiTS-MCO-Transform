package org.titsmcoteam;

import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

import org.titsmcoteam.CLIOpts;
import org.titsmcoteam.astanalyzers.CustomPCAnalyzer;
import org.titsmcoteam.astanalyzers.IncludesAnalyzer;
import org.titsmcoteam.astanalyzers.PerkAnalyzer;
import org.yaml.snakeyaml.Yaml;

import com.beust.jcommander.JCommander;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import macromedia.asc.parser.*;
import macromedia.asc.util.Context;
import macromedia.asc.util.ContextStatics;
import macromedia.asc.util.ObjectList;
import flash.swf.tools.SyntaxTreeDumper;
import me.venturega.asanalyze.evaluators.ParentizerEvaluator;
import me.venturega.asanalyze.utils.AS3PrettyPrinter;
import me.venturega.asanalyze.utils.AstAnalyzer;
import me.venturega.asanalyze.utils.CoffeePrinter;
import me.venturega.asanalyze.utils.GlobalConfiguration;

public class App {
	private static Context cx;
	private static ContextStatics statics;
	private static ParentizerEvaluator parents;
	private final static Logger LOGGER = Logger.getLogger(App.class.getName());
	static ArrayList<AstAnalyzer> astAnalyzers = new ArrayList<AstAnalyzer>();
	

	public static void main(String[] args) throws IOException {
		astAnalyzers.add((AstAnalyzer)(new PerkAnalyzer()));
		astAnalyzers.add((AstAnalyzer)(new CustomPCAnalyzer()));
		
		CLIOpts cli_opts = new CLIOpts();
		JCommander jc = new JCommander(cli_opts, args);

		if(cli_opts.help) {
			jc.usage();
			return;
		}

		if (cli_opts.options != null) {
			for (String chunk : cli_opts.options) {
				String[] chunks = chunk.split("=");
				GlobalConfiguration.put(chunks[0], chunks[1]);
			}
		}
		
		if(cli_opts.includes != null) {
			IncludesAnalyzer ia = new IncludesAnalyzer();
			ia.loadInjectionSpecs(cli_opts.includes);
			astAnalyzers.add(ia);
			LOGGER.info("IncludesAnalyzer activated!");
		}

		Boolean wrotestuff=false;

		PrintWriter pw;
		for(String path : cli_opts.as3files) {
			File subject = new File(path);
			if (subject.isFile()) {
				ProgramNode program = parseFile(cli_opts, path);
				if (cli_opts.xmlfile != "") {
					LOGGER.info(String.format("Writing %s...", cli_opts.xmlfile));
					pw = new PrintWriter(cli_opts.xmlfile);
					SyntaxTreeDumper std = new SyntaxTreeDumper(pw);
					program.evaluate(getContext(), std);
					pw.close();
					wrotestuff=true;
				}

				if (cli_opts.coffeefile != "") {
					LOGGER.info(String.format("Writing %s...", cli_opts.coffeefile));
					pw = new PrintWriter(cli_opts.coffeefile);
					CoffeePrinter cp = new CoffeePrinter(pw);
					program.evaluate(getContext(), cp);
					pw.close();
					wrotestuff=true;
				}

				if (cli_opts.asfile != "") {
					LOGGER.info(String.format("Writing %s...", cli_opts.asfile));
					pw = new PrintWriter(cli_opts.asfile);
					AS3PrettyPrinter pp = new AS3PrettyPrinter(pw);
					program.evaluate(getContext(), pp);
					if(cli_opts.trace) {
						LOGGER.info("TRACE enabled.");
						pp.TRACE=true;
					}
					pw.close();
					wrotestuff=true;
				}

				if(wrotestuff)
					return;
			}
			else if(subject.isDirectory()){
				Files.walk(Paths.get(path)).filter(Files::isRegularFile).forEach((Path p) -> {
					if(p.toString().endsWith(".as")) {
						LOGGER.info(String.format("Processing %s...", p.toString()));
						try {
							ProgramNode program = parseFile(cli_opts, p.toString());
							//LOGGER.info(String.format("Writing %s...", cli_opts.asfile));
							PrintWriter _pw = new PrintWriter(p.toString());
							AS3PrettyPrinter pp = new AS3PrettyPrinter(_pw);
							if(cli_opts.trace) {
								LOGGER.info("TRACE enabled.");
								//String lcfilename = p.toString().toLowerCase();
								//pp.TRACE=lcfilename.endsWith("seraxpreg1.as") || lcfilename.endsWith("sandworm.as");
								pp.TRACE = true;
							}
							program.evaluate(getContext(), pp);
							_pw.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					});
			} else {
				LOGGER.severe(String.format("Unable to find file %s!", subject));
				return;
			}
		}
		if(!cli_opts.no_processing) {
			for(AstAnalyzer asta : astAnalyzers) {
				asta.PostProcess();
			}
		}
	}

	private static ProgramNode parseFile(CLIOpts cli_opts, String path) throws IOException {
		boolean isInclude = false;
		if(path.contains("includes")) {
			isInclude = true;
			LOGGER.info("Parsing as include!");
		}
//		LOGGER.info(String.format("Parsing %s...",path));
		ProgramNode program = ParseFile(path, isInclude, cli_opts.dump_nsconfig);

		//LOGGER.info("Calculating AST heritage...");
		parents = new ParentizerEvaluator();
		program.evaluate(program.cx, parents);

		if (!cli_opts.no_processing)
			ProcessEntities(program, getContext());

		return program;
	}

	private static void ProcessEntities(ProgramNode pn, Context cx) {
		if (pn == null) {
			LOGGER.severe("FAILED TO PARSE. ProgramNode is null!");
			return;
		}
		for (Object stmt : pn.statements.items.toArray()) {
			if (stmt instanceof ClassDefinitionNode) {
				ClassDefinitionNode cls = (ClassDefinitionNode) stmt;
				if (cls.statements != null && cls.statements.items != null) {
					for (Node clsstmt : cls.statements.items) {
						if (!(clsstmt instanceof FunctionDefinitionNode))
							continue;
						FunctionDefinitionNode fnc = (FunctionDefinitionNode) clsstmt;
						for (AstAnalyzer aa : astAnalyzers) {
							aa.parents = parents;
							aa.setContext(cx);
							aa.ProcessMethod(cls.name.name, fnc.name.identifier.name, cls, fnc);
						}
					}
				}
			} else if (stmt instanceof FunctionDefinitionNode) {
				FunctionDefinitionNode fnc = (FunctionDefinitionNode) stmt;
				for (AstAnalyzer aa : astAnalyzers) {
					aa.parents = parents;
					aa.setContext(cx);
					aa.ProcessMethod(null, fnc.name.identifier.name, null, fnc);
				}
			}
		}
		for (AstAnalyzer aa : astAnalyzers) {
			aa.PostProcess();
		}
	}

	public static ProgramNode ParseFile(String filename, boolean isInclude, String dumpConfigNamespacesTo) throws IOException {
		statics = new ContextStatics();
		setContext(new Context(statics));
		cx.setPath(Paths.get(filename).getParent().toAbsolutePath().toString());
		cx.scriptAssistParsing = true; // No parsing included files pls
		FileInputStream is = null;
		try {
			is = new FileInputStream(filename);
			//public Parser(Context cx, InputStream in, String origin, String encoding, boolean emit_doc_info, boolean save_comment_nodes, IntList block_kind_stack, boolean is_include)
			Parser parser = new Parser(getContext(), is, filename, null, true, true, null, isInclude);
			if(isInclude) {
				// /shrug
				parser.config_namespaces.add(new HashSet<String>());
				parser.config_namespaces.at(0).add("classes");
				parser.config_namespaces.at(0).add("TiTS");
			}
			ProgramNode pn = parser.parseProgram();
			//LOGGER.info("Parsed.");
			return pn;
		} finally {
			if (is != null)
				is.close();
		}

	}

	public static Context getContext() {
		return cx;
	}

	public static void setContext(Context cx) {
		App.cx = cx;
	}

}
