package org.titsmcoteam;

import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.Parameter;

public class CLIOpts {
	@Parameter(required = true)
	public List<String> as3files = new ArrayList<>();

	@Parameter(names = "--as-xml", description = "Dump AST Tree to XML")
	public String xmlfile = "";

	@Parameter(names = "--as-coffee", description = "Convert to CoffeeScript")
	public String coffeefile = "";

	@Parameter(names = "--as-as3", description = "Convert to AS3")
	public String asfile = "";

	@Parameter(names = "--set", description = "Set GlobalConfiguration variables.")
	public List<String> options;

	@Parameter(names = "--skip-processing", description = "Just parse, don't process.")
	public boolean no_processing=false;

	@Parameter(names = "--trace", description = "Do not enable this.")
	public boolean trace = false;

    @Parameter(names = "--help", help = true)
    public boolean help = false;

    @Parameter(names = "--dump-nsconfig", description="Dump parser post-parse namespace configuration to the specified file.")
	public String dump_nsconfig = null;

    @Parameter(names = "--includes", description="Load include injections from a YAML file. Activates IncludesAnalyzer.")
	public String includes = null;
}