package org.titsmcoteam.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Perk extends ASObject {
	public class ValueStorageInfo {
		int ID = 0;
		String Name = "";
		String Description = "";
		public void deserialize(Map<String, Object> _vsi) {
			Name = (String) _vsi.get("name");
			Description = (String) _vsi.getOrDefault("description", "");
		}
		public Object serialize() {
			HashMap<String, Object> data = new LinkedHashMap<String, Object>();
			data.put("name", Name);
			data.put("description", Description);
			return data;
		}
	}
	public String ShortName = "";
	public String Description = "";
	public Boolean AutoGained = false;
	public int ClassLimit = -1;
	public int LevelLimit = -1;
	public boolean LimitAccess = false;
	public String LimitAccessFunction = "";
	public ValueStorageInfo[] ValueStorage = new ValueStorageInfo[4];
	
	/**
	 * Whether this Perk was generated automatically from Fencode.
	 */
	public Boolean AutoDetected = true;
	
	public void setupFromKeyname(String newID, String origName) {	
		//this.ID=ASObject.normName(newID);
		this.ID=newID;
		if(!AllNames.contains(ID))
			this.AllNames.add(ID);
		if(!AllNames.contains(origName))
			this.AllNames.add(origName);
		this.Name=origName;
	}

	public void deserialize(String id, Map<String, Object> value) {
		AutoDetected=false;
		
		ID = id;
		AllNames = (ArrayList<String>) value.get("all-names");
		Name = (String) value.get("name");
		ShortName = (String) value.getOrDefault("short-name", "");
		Description = (String)value.getOrDefault("description", "");
		AutoGained = (Boolean)value.getOrDefault("auto-gained", false);
		LimitAccess = (Boolean)value.getOrDefault("limit-access", false);
		ClassLimit = (Integer)value.getOrDefault("class-limit", -1);
		LevelLimit = (Integer)value.getOrDefault("level-limit", -1);
		int i=0;
		for(Object _vsi : (ArrayList<Object>)value.getOrDefault("value-storage", new ArrayList<Object>())) {
			if(_vsi instanceof Map<?,?>) {
				ValueStorageInfo vsi = new ValueStorageInfo();
				vsi.ID=i;
				vsi.deserialize((Map<String, Object>)_vsi);
				this.ValueStorage[i++] = vsi;
			}
			if(_vsi instanceof String) {
				ValueStorageInfo vsi = new ValueStorageInfo();
				vsi.ID=i;
				vsi.Name = (String)_vsi;
				this.ValueStorage[i++] = vsi;
			}
		}
	}

	public Object serialize() {
		HashMap<String, Object> data = new LinkedHashMap<String, Object>();
		data.put("name", Name);
		data.put("all-names", AllNames);
		data.put("short-name", ShortName);
		data.put("description", Description);
		data.put("auto-gained", AutoGained);
		data.put("limit-access", LimitAccess);
		data.put("class-limit", ClassLimit);
		data.put("level-limit", LevelLimit);
		ArrayList<Object> valueStorage = new ArrayList<Object>();
		for(ValueStorageInfo vsi : ValueStorage) {
			if(vsi != null)
				valueStorage.add(vsi.serialize());
			else
				valueStorage.add(null);
		}
		data.put("value-storage", valueStorage);
		return data;
	}
}
