package org.titsmcoteam.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.titsmcoteam.game.Perk.ValueStorageInfo;

import macromedia.asc.parser.Node;

public class CustomPC extends ASObject {
	private boolean AutoDetected;
	private String ShortName;
	private String Description;
	public ArrayList<Node> nodes = new ArrayList<Node>();

	public void setupFromKeyname(String value, String origName) {	
		this.ID=ASObject.normName(value);
		if(!AllNames.contains(ID))
			this.AllNames.add(ID);
		if(!AllNames.contains(origName))
			this.AllNames.add(origName);
		this.Name=origName;
	}

	public void deserialize(String id, Map<String, Object> value) {
		AutoDetected=false;
		
		ID = id;
		AllNames = (ArrayList<String>) value.get("all-names");
		Name = (String) value.get("name");
		ShortName = (String) value.getOrDefault("short-name", "");
		Description = (String)value.getOrDefault("description", "");
	}

	public Object serialize() {
		HashMap<String, Object> data = new LinkedHashMap<String, Object>();
		data.put("name", Name);
		data.put("all-names", AllNames);
		data.put("short-name", ShortName);
		data.put("description", Description);
		return data;
	}

	public void addCaseLabel(String label) {
		if(this.ID == null || this.ID == "")
			this.ID = ASObject.normName(label);
		if(!AllNames.contains(ID))
			this.AllNames.add(ID);
		if(!AllNames.contains(label))
			this.AllNames.add(label);
	}

	public void addNode(Node n) {
		this.nodes.add(n);
	}
}
