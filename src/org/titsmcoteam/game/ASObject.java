package org.titsmcoteam.game;

import java.util.ArrayList;

public class ASObject {

	private static final String VALID_ID_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 ";
	private static final String VALID_CONSTNAME_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ";
	
	public String ID = "";
	public String Name = "";
	public ArrayList<String> AllNames = new ArrayList<String>();
	public ArrayList<String> Imports = new ArrayList<String>();
	
	public static String normName(String inp) {
		String o = "";
		char c;
		for(int i =0;i<inp.length();i++) {
			c=inp.charAt(i);
			if(ASObject.VALID_ID_CHARS.indexOf(c) == -1) {
				continue;
			}
			o += c;
		}
		return o;
	}

	public String getConstName() {
		String o = "";
		char c;
		for(int i =0;i<this.ID.length();i++) {
			c=this.ID.toUpperCase().charAt(i);
			if(ASObject.VALID_CONSTNAME_CHARS.indexOf(c) == -1) {
				//System.out.println(String.format("Skip %s", c));
				continue;
			}
			//System.out.println(String.format("Keep %s", c));
			o += c;
		}
		return o.replace(" ", "_");
	}
}
