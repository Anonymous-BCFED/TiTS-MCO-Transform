package org.titsmcoteam.astanalyzers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.logging.Logger;

import org.titsmcoteam.game.CustomPC;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.DumperOptions.FlowStyle;

import macromedia.asc.parser.BreakStatementNode;
import macromedia.asc.parser.ClassDefinitionNode;
import macromedia.asc.parser.FunctionDefinitionNode;
import macromedia.asc.parser.IdentifierNode;
import macromedia.asc.parser.ImportDirectiveNode;
import macromedia.asc.parser.LiteralStringNode;
import macromedia.asc.parser.CaseLabelNode;
import macromedia.asc.parser.Node;
import macromedia.asc.parser.PackageDefinitionNode;
import macromedia.asc.parser.PackageIdentifiersNode;
import macromedia.asc.parser.PackageNameNode;
import macromedia.asc.parser.ProgramNode;
import macromedia.asc.parser.SwitchStatementNode;
import macromedia.asc.util.Context;
import macromedia.asc.util.ContextStatics;
import me.venturega.asanalyze.utils.AS3PrettyPrinter;
import me.venturega.asanalyze.utils.AstAnalyzer;

public class CustomPCAnalyzer extends AstAnalyzer {

	private ArrayList<String> unidentifiedCustomPCs = new ArrayList<String>();
	private HashMap<String, CustomPC> customPCs = new HashMap<String, CustomPC>();
	private HashMap<String, String> nameMappings = new HashMap<String, String>();
	private final String CONFIG_FILE_NAME = "data/mappings/custom_PCs.yml";
	private final String CUSTOMPC_FILE_NAME = "data/custom_PCs.yml";
	private final String FOUND_CUSTOMPC_FILE_NAME = "data/found_custom_PCs.yml";
	private final String FOUND_CUSTOMPC_BASEDIR = "data/found_custom_PC_code/";

	private final static Logger LOGGER = Logger.getLogger(CustomPCAnalyzer.class.getName());

	public CustomPCAnalyzer() {
		nameMappings = loadNameMappings(CONFIG_FILE_NAME);
		loadCustomPCsFrom(CUSTOMPC_FILE_NAME);
	}

	private void loadCustomPCsFrom(String filename) {
		File cfgFile = new File(filename);
		if (cfgFile.isFile()) {
			Yaml yml = new Yaml();
			InputStreamReader input = null;
			try {
				input = new InputStreamReader(new FileInputStream(filename), "UTF8");

				Map<String, Object> m = yml.load(input);
				for(Map.Entry<String, Object> e: m.entrySet()) {
					Object value = e.getValue();
					if(value instanceof Map<?, ?>)
					{
						CustomPC p = new CustomPC();
						p.deserialize(e.getKey(), (Map<String, Object>)value);
						customPCs.put(p.ID, p);
					}
				}

			} catch (FileNotFoundException | UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}

	@Override
	public void ProcessMethod(String clsName, String fncName, ClassDefinitionNode cls, FunctionDefinitionNode fnc) {
		if(fncName == "customPCCheck") {
			this.TransformRecursive(fnc, new Function<Node, Node>() {
				public Node apply(Node N) {
					if (N instanceof SwitchStatementNode) {
						SwitchStatementNode sw = (SwitchStatementNode) N;
						if(sw.expr instanceof IdentifierNode && ((IdentifierNode)sw.expr).name == "short") {
							processSwitch(sw);
						}
	
					}
					return N;
				}
			});
		}
	}

	private void processSwitch(SwitchStatementNode sw) {
		CustomPC current = null;
		for(int i=0, size = sw.statements.items.size();i<size;i++) {
			Node n = sw.statements.items.get(i);
			if(n!=null) {
				if (n instanceof CaseLabelNode) {
					CaseLabelNode c = (CaseLabelNode)n;
					if(c.label instanceof LiteralStringNode) {
						if(current == null)
							current = new CustomPC();
						current.addCaseLabel(((LiteralStringNode)c.label).value);
					}
				}
				else if(n instanceof BreakStatementNode) {
					this.customPCs.put(current.ID, current);
					current = null;
				} else {
					current.addNode(n);
				}
			}
		}
	}

	private void ensureImportExists(ProgramNode program, String importID) {
		ArrayList<String> allImports = new ArrayList<String>();
		int indexOfLastImport = 0;
		PackageDefinitionNode pkg=null;
		for(int i =0;i<program.statements.items.size();i++) {
			Node n = program.statements.items.get(i);
			if(n instanceof ImportDirectiveNode) {
				PackageNameNode name = ((ImportDirectiveNode)n).name;
				String iid="";
				for (int j = 0; j < name.id.list.size(); j++) {
					if (j > 0) {
						iid+=".";
					}
					iid+=(name.id.list.get(j).name);
				}
				allImports.add(iid);
				//System.out.println(iid);
				indexOfLastImport = i+1;
			}
		}
		if(program.pkgdefs!=null && !program.pkgdefs.isEmpty()) {
			pkg = program.pkgdefs.get(0);
		}
		if(!allImports.contains(importID)) {
			PackageNameNode pnn = new PackageNameNode(new PackageIdentifiersNode(new IdentifierNode(importID, 0), 0, true), 0);
			ImportDirectiveNode idn = new ImportDirectiveNode(null, null, pnn, null, cx);
			program.statements.items.add(indexOfLastImport, idn);
			LOGGER.info(String.format("Added import %s.", importID));
		}
	}

	private ProgramNode getProgramNode(Node curNode) {
		//System.out.println(curNode.getClass().getCanonicalName());
		while(!(curNode instanceof ProgramNode)) {
			curNode = this.parents.getParentOf(curNode);
			if(curNode == null)
				break;
			//System.out.println(curNode.getClass().getCanonicalName());
		}
		return (ProgramNode) curNode;
	}

	@Override
	public void PostProcess() {
		Map<String, Object> cpcMap = new HashMap<String, Object>();
		CustomPC c;
		for(String ID : unidentifiedCustomPCs) {
			c = customPCs.get(ID);
			cpcMap.put(c.ID, c.serialize());
		}
		File foundPerksFN = new File(FOUND_CUSTOMPC_FILE_NAME);
		File dataDir = foundPerksFN.getParentFile();
		if (!dataDir.isDirectory()) {
			LOGGER.info("Creating dataDir: "+dataDir.getPath());
			dataDir.mkdirs();
		}
		try {
			FileWriter f = new FileWriter(FOUND_CUSTOMPC_FILE_NAME);
			DumperOptions _do = new DumperOptions();
			_do.setDefaultFlowStyle(FlowStyle.BLOCK);
			Yaml yaml = new Yaml(_do);
			yaml.dump(cpcMap,f);
			f.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		File found_cpcdir = new File(FOUND_CUSTOMPC_BASEDIR);
		if(!found_cpcdir.isDirectory()) {
			LOGGER.info("Creating custom PC code directory: "+found_cpcdir.getPath());
			found_cpcdir.mkdirs();
		}
		Path cpcFile;
		PrintWriter pw = null;
		AS3PrettyPrinter pp;
		ContextStatics statics = new ContextStatics();
		setContext(new Context(statics));
		//cx.setPath(Paths.get(filename).getParent().toAbsolutePath().toString());
		cx.scriptAssistParsing = true; // No parsing included files pls
		for(String ID : this.unidentifiedCustomPCs) {
			c = customPCs.get(ID);
			cpcFile = Path.of(found_cpcdir.getAbsolutePath(), ID+".as");
			LOGGER.info(String.format("Writing %s...", cpcFile.toString()));
			try {
				pw = new PrintWriter(cpcFile.toString());
				pp = new AS3PrettyPrinter(pw);
				for (Node n : c.nodes) {
					n.evaluate(cx, pp);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if (pw != null)
					pw.close();
			}
		}
	}

}
