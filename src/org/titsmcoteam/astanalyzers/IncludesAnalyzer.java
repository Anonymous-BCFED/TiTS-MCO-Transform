package org.titsmcoteam.astanalyzers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.function.Function;
import java.util.logging.Logger;

import org.yaml.snakeyaml.Yaml;

import macromedia.asc.parser.ClassDefinitionNode;
import macromedia.asc.parser.FunctionDefinitionNode;
import macromedia.asc.parser.IdentifierNode;
import macromedia.asc.parser.IncludeDirectiveNode;
import macromedia.asc.parser.LiteralStringNode;
import macromedia.asc.parser.Node;
import macromedia.asc.parser.StatementListNode;
import macromedia.asc.parser.SwitchStatementNode;
import me.venturega.asanalyze.utils.AstAnalyzer;

public class IncludesAnalyzer extends AstAnalyzer {
	public class IncludeInjectionSpec {
		public String inFile;
		public String inClass;
		public String inFunction="";
		public String injectedFile;
		public String after = null;
		public String before = null;
		
		public boolean written = false;
		
		public void deserialize(String key, Map<String, Object> data) {
			Object in = data.get("in");
			if(in instanceof String) {
				inFile = (String)in;
				inClass = "";
				inFunction = "";
			} else {
				Map<String, Object> _in = (Map<String, Object>)in;
				inFile = (String)_in.get("file");
				inClass = (String)_in.get("class");
				if(_in.containsKey("function"))
					inFunction = (String)_in.get("function");
			}
			injectedFile = key;
			if(data.containsKey("after"))
				after=(String) data.get("after");
			if(data.containsKey("before"))
				before=(String) data.get("before");
		}
	}

	private final static Logger LOGGER = Logger.getLogger(IncludesAnalyzer.class.getName());
	
	private ArrayList<IncludeInjectionSpec> injectionSpecs = new ArrayList<IncludeInjectionSpec>();
	
	public IncludesAnalyzer() {
		
	}

	public void loadInjectionSpecs(String filename) {
	    InputStream input = null;
	    Map<String, Object> specs;
		try {
			input = new FileInputStream(new File(filename));
			Yaml yml = new Yaml();
			specs = yml.load(input);
			for(String injectedFile : specs.keySet()) {
				IncludeInjectionSpec spec = new IncludeInjectionSpec();
				injectionSpecs.add(spec);
				spec.deserialize(injectedFile, (Map<String, Object>) specs.get(injectedFile));
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				input.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void ProcessMethod(String clsName, String fncName, ClassDefinitionNode cls, FunctionDefinitionNode fnc) {
		if(injectionSpecs.size() == 0) return;
		IncludeDirectiveNode lastIDN = null;
		for(Object N : cls.statements.items.toArray()) {
			if (N instanceof IncludeDirectiveNode) {
				IncludeDirectiveNode inc = (IncludeDirectiveNode) N;
				lastIDN = inc;
				for(IncludeInjectionSpec spec : injectionSpecs) {
					if(spec.written) continue;
					if(spec.inClass != cls.name.name) continue;
					if(spec.inFunction == null) continue;
					if(spec.after == inc.original_filespec.value || spec.before == inc.original_filespec.value) {
						StatementListNode parent = (StatementListNode) parents.getParentOf(inc);
						int np = parent.items.indexOf(inc);
						IncludeDirectiveNode idn = new IncludeDirectiveNode(cx, new LiteralStringNode(spec.injectedFile), null);
						if(spec.after != null) {
							parent.items.add(np+1, idn);
						} else {
							parent.items.add(np, idn);
						}
						spec.written = true;
					}
				}

			}
		}
		for(IncludeInjectionSpec spec : injectionSpecs) {
			if(spec.written) continue;
			if(spec.inClass != cls.name.name) continue;
			if(spec.inFunction == null) continue;
			if(spec.after == null && spec.before == null) continue;
			int np = cls.statements.items.indexOf(lastIDN);
			IncludeDirectiveNode idn = new IncludeDirectiveNode(cx, new LiteralStringNode(spec.injectedFile), null);
			if (np == -1) {
				np = 0;
			} else {
				np++;
			}
			cls.statements.items.add(np, idn);
			spec.written = true;
		}
	}

}
