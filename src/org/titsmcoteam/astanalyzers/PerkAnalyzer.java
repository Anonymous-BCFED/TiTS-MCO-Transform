package org.titsmcoteam.astanalyzers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.logging.Logger;

import org.titsmcoteam.game.ASObject;
import org.titsmcoteam.game.Perk;

import macromedia.asc.parser.ArgumentListNode;
import macromedia.asc.parser.BinaryExpressionNode;
import macromedia.asc.parser.CallExpressionNode;
import macromedia.asc.parser.ClassDefinitionNode;
import macromedia.asc.parser.ConditionalExpressionNode;
import macromedia.asc.parser.FunctionDefinitionNode;
import macromedia.asc.parser.GetExpressionNode;
import macromedia.asc.parser.IdentifierNode;
import macromedia.asc.parser.ImportDirectiveNode;
import macromedia.asc.parser.ListNode;
import macromedia.asc.parser.LiteralBooleanNode;
import macromedia.asc.parser.LiteralNumberNode;
import macromedia.asc.parser.LiteralStringNode;
import macromedia.asc.parser.MemberExpressionNode;
import macromedia.asc.parser.Node;
import macromedia.asc.parser.PackageDefinitionNode;
import macromedia.asc.parser.PackageIdentifiersNode;
import macromedia.asc.parser.PackageNameNode;
import macromedia.asc.parser.ProgramNode;
import macromedia.asc.parser.ThisExpressionNode;
import macromedia.asc.parser.UnaryExpressionNode;
import macromedia.asc.parser.VariableBindingNode;
import me.venturega.asanalyze.utils.ASTUtils;
import me.venturega.asanalyze.utils.AstAnalyzer;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.DumperOptions.FlowStyle;
import org.yaml.snakeyaml.Yaml;

public class PerkAnalyzer extends AstAnalyzer {
	private ArrayList<String> unidentifiedPerks = new ArrayList<String>();
	private HashMap<String, Perk> perks = new HashMap<String, Perk>();
	private HashMap<String, String> nameMappings = new HashMap<String, String>();
	private final String CONFIG_FILE_NAME = "data/mappings/perks.yml";
	private final String PERK_FILE_NAME = "data/perks.yml";
	private final String FOUND_PERK_FILE_NAME = "data/found_perks.yml";

	private final static Logger LOGGER = Logger.getLogger(PerkAnalyzer.class.getName());

	public PerkAnalyzer() {
		nameMappings = loadNameMappings(CONFIG_FILE_NAME);
		loadPerksFrom(PERK_FILE_NAME);
	}

	private void loadPerksFrom(String filename) {
		File cfgFile = new File(filename);
		if (cfgFile.isFile()) {
			Yaml yml = new Yaml();
			InputStreamReader input = null;
			try {
				input = new InputStreamReader(new FileInputStream(filename), "UTF8");

				Map<String, Object> m = yml.load(input);
				for(Map.Entry<String, Object> e: m.entrySet()) {
					Object value = e.getValue();
					if(value instanceof Map<?, ?> && e.getKey() != "")
					{
						Perk p = new Perk();
						p.deserialize(e.getKey(), (Map<String, Object>)value);
						perks.put(p.ID, p);
					}
				}

			} catch (FileNotFoundException | UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}

	@Override
	public void ProcessMethod(String clsName, String fncName, ClassDefinitionNode cls, FunctionDefinitionNode fnc) {
		this.FindRecursive(fnc, new Function<Node, Node>() {
			public Node apply(Node N) {
				if (N instanceof CallExpressionNode) {
					CallExpressionNode cen = (CallExpressionNode) N;
					if (cen.getIdentifier() != null) {
						return processCall(cen);
					}

				}
				return N;
			}
		});
	}

	private Node processCall(CallExpressionNode cen) {
		// System.out.println(cen.getIdentifier().name);
		switch (cen.getIdentifier().name) {
		case "createPerk":
			return processCreatePerkCall(cen);
		case "hasPerk":
			return processHasPerkCall(cen);
		}
		return cen;
	}

	private Node processHasPerkCall(CallExpressionNode cen) {
		/*
		public function hasPerk(perkName: String, ignoreHidden:Boolean = false): Boolean
		*/

		// We need arg[0] to be a literal string.
		if (!(cen.args.items.get(0) instanceof LiteralStringNode)) {
			return cen;
		}

		//System.out.println("[PerkAnalyzer::processHasPerkCall] Found " + ((LiteralStringNode) cen.args.items.get(0)).value + "!");
		//LOGGER.info("Found " + ((LiteralStringNode) cen.args.items.get(0)).value + "!");

		Perk p = new Perk();
		ArrayList<Node> args = new ArrayList<Node>();
		if (cen.args.items.size() > 1) {
			for (int i = 1; i < Math.min(2, cen.args.items.size()); i++) {
				args.add(cen.args.items.get(i));
			}
		}
		for (int i = 0; i < cen.args.items.size(); i++) {
			switch (i) {
			case 0:
				p = getOrCreatePerk(((LiteralStringNode) cen.args.items.get(0)).value);
				break;
			//case 1:
			//	args.set(i - 1, cen.args.items.get(i));
			//	break;
			}
		}
		
		String constName = p.getConstName();
		
		//System.out.println(String.format("hasPerk: constName=%s, p.ID=%s", constName, p.ID));

		// FlagSetup(); ->
		// <ExpressionStatementNode>
		//   <ListNode>
		//     <MemberExpressionNode>
		//       <selector>
		//         <CallExpressionNode is_new="false" is_package="false" isRValue="false" isAttr="false" isSuper="false" isThis="false" isVoidResult="false" mode="lexical">
		//           <IdentifierNode name="FlagSetup"/>
		//         </CallExpressionNode>
		//       </selector>
		//     </MemberExpressionNode>
		//   </ListNode>
		// </ExpressionStatementNode>
		MemberExpressionNode mxn = (MemberExpressionNode) parents.getParentOf(cen);
		MemberExpressionNode root_mxn = ASTUtils.getRootMemberExpressionNode(parents, mxn);

		MemberExpressionNode mxnPerkRegistry = new MemberExpressionNode(null,
				new GetExpressionNode(new IdentifierNode("PerkRegistry", false, 0)), 0);
		MemberExpressionNode mxnPerkID = new MemberExpressionNode(mxnPerkRegistry,
				new GetExpressionNode(new IdentifierNode(constName, false, 0)), 0);

		ArgumentListNode aln = new ArgumentListNode(null, 0);
		aln.items.clear();
		aln.items.add(ASTUtils.stripMethodFromMXN(root_mxn, new ThisExpressionNode()));
		for (int i = 0; i < args.size(); i++) {
			if (args.get(i) == null) {
				aln.items.add(new LiteralNumberNode("0"));
			} else {
				aln.items.add(args.get(i));
			}
		}

		CallExpressionNode nucen = new CallExpressionNode(new IdentifierNode("hasPerk", 0), aln);
		MemberExpressionNode newmxn = new MemberExpressionNode(mxnPerkID, nucen, 0);
		Node parent = parents.getParentOf(root_mxn);
		ProgramNode program = null;
		if (parent instanceof ListNode) {
			ListNode line = (ListNode) parent;
			line.items.set(0, newmxn);
			program = getProgramNode(line);
		} else if (parent instanceof UnaryExpressionNode) {
			UnaryExpressionNode uxn = (UnaryExpressionNode)parent;
			uxn.expr=newmxn;
			uxn.setPosition(0);
			program = getProgramNode(uxn);
		} else if (parent instanceof ConditionalExpressionNode) {
			ConditionalExpressionNode cxn = (ConditionalExpressionNode)parent;
			if(mxn==cxn.condition) {
				cxn.condition = newmxn;
			}
			else if(mxn == cxn.elseexpr) {
				cxn.elseexpr = newmxn;
			}
			else if(mxn == cxn.thenexpr) {
				cxn.thenexpr = newmxn;
			}
			else {
				LOGGER.severe("COULD NOT DETERMINE ConditionalExpressionNode PROPERTY!");
			}
			//uxn.setPosition(0);
			program = getProgramNode(cxn);
		} else if (parent instanceof BinaryExpressionNode) {
			BinaryExpressionNode bxn = (BinaryExpressionNode)parent;
			if(mxn==bxn.lhs) {
				bxn.lhs = newmxn;
			}
			else if(mxn == bxn.rhs) {
				bxn.rhs = newmxn;
			}
			else {
				LOGGER.severe("COULD NOT DETERMINE BinaryExpressionNode PROPERTY!");
			}
			//bxn.setPosition(0);
			program = getProgramNode(bxn);
		} else if (parent instanceof VariableBindingNode) {
			VariableBindingNode vbn = (VariableBindingNode)parent;
			vbn.initializer = newmxn;
			//bxn.setPosition(0);
			program = getProgramNode(vbn);
		} else if (parent instanceof ArgumentListNode) {
			ArgumentListNode _aln = (ArgumentListNode)parent;
			boolean found = false;
			for(int i =0;i<_aln.items.size();i++) {
				if(mxn == _aln.items.get(i)) {
					found = true;
					_aln.items.set(i, newmxn);
					//_aln.items.add(new IdentifierNode("/* MCO-Transform: Set #"+Integer.toString(i)+" to newmxn. */",0));
				}
			}
			if(!found) {
				_aln.items.add(new IdentifierNode("/* MCO-Transform: Could not find match with MXN in ALN. */",0));
			}
			_aln.setPosition(0);
			program = getProgramNode(_aln);
		} else if(parent == null) {
			LOGGER.severe("root_mxn PARENT == null! "+root_mxn.pos());
		} else {
			LOGGER.severe("UNKNOWN root_mxn PARENT: "+parent.getClass().getCanonicalName());
		}
		if(program!=null)
			ASTUtils.ensureImportExists(program, "classes.GameData.PerkRegistry");
		return null;
	}

	private String perkCodeName2ID(String codeName) {
		if(nameMappings.containsKey(codeName))
			return nameMappings.get(codeName);
		return codeName;
	}

	private Perk getOrCreatePerk(String origID) {
		String ID=origID;
		Perk p;
		ID = perkCodeName2ID(ASObject.normName(ID));
		if(ID == "" || ID == null) {
			ID = ASObject.normName(origID);
		}
		if (perks.containsKey(ID))
			return perks.get(ID);
		else {
			LOGGER.info(String.format("Found undefined perk %s (origID=%s)!", ID, origID));
			p = new Perk();
			p.AutoDetected = true;
			p.setupFromKeyname(ID, origID);
			perks.put(p.ID, p);
			unidentifiedPerks.add(p.ID);
			return p;
		}
	}

	/**
	 * pc.createPerk("Butts", 0, 0, 0, 0, "Big, meaty butts.") -> PerkRegistry.BUTTS.applyTo(pc, 0, 0, 0, 0);
	 *
	 * @param cen
	 * @return
	 */
	private Node processCreatePerkCall(CallExpressionNode cen) {
		// @formatter: off
		/*
		//Create a perk
		public function createPerk(keyName: String, value1: Number = 0, value2: Number = 0, value3: Number = 0, value4: Number = 0, desc: String = ""): void
		0: keyName
		1: value1 = 0
		2: value2 = 0
		3: value3 = 0
		4: value4 = 0
		5: desc = ""
		*/
		// @formatter: on

		// We need arg[0] to be a literal string.
		if (!(cen.args.items.get(0) instanceof LiteralStringNode)) {
			return cen;
		}

//		System.out.println("[PerkAnalyzer::processCreatePerkCall] Found " + ((LiteralStringNode) cen.args.items.get(0)).value + "!");
		//LOGGER.info("Found " + ((LiteralStringNode) cen.args.items.get(0)).value + "!");

		Perk p = new Perk();
		ArrayList<Node> args = new ArrayList<Node>();
		if (cen.args.items.size() > 1) {
			for (int i = 0; i < Math.min(4, cen.args.items.size() - 1); i++) {
				args.add(null);
			}
		}
		for (int i = 0; i < cen.args.items.size(); i++) {
			switch (i) {
			case 0:
				p = getOrCreatePerk(((LiteralStringNode) cen.args.items.get(0)).value);
				break;
			case 1:
			case 2:
			case 3:
			case 4:
				//if (cen.args.items.get(i) instanceof LiteralNumberNode) {
				//	p.ValueStorage[i - 1] = ((LiteralNumberNode) cen.args.items.get(i)).intValue();
				//}
				args.set(i - 1, cen.args.items.get(i));
				break;
			case 5:
				if (cen.args.items.get(i) instanceof LiteralStringNode) {
					p.Description = ((LiteralStringNode) cen.args.items.get(i)).value;
				}
				break;
			}
		}
		
		String constName = p.getConstName();

		//System.out.println(String.format("createPerk(): constName=%s, p.ID=%s", constName, p.ID));

		// FlagSetup(); ->
		// <ExpressionStatementNode>
		// <ListNode>
		// <MemberExpressionNode>
		// <selector>
		// <CallExpressionNode is_new="false" is_package="false"
		// isRValue="false" isAttr="false" isSuper="false" isThis="false"
		// isVoidResult="false" mode="lexical">
		// <IdentifierNode name="FlagSetup"/>
		// </CallExpressionNode>
		// </selector>
		// </MemberExpressionNode>
		// </ListNode>
		// </ExpressionStatementNode>
		MemberExpressionNode mxn = (MemberExpressionNode) parents.getParentOf(cen);
		MemberExpressionNode root_mxn = ASTUtils.getRootMemberExpressionNode(parents, mxn);
		//ListNode lst = (ListNode) parents.getParentOf(root_mxn);

		MemberExpressionNode mxnPerkRegistry = new MemberExpressionNode(null,
				new GetExpressionNode(new IdentifierNode("PerkRegistry", false, 0)), 0);
		MemberExpressionNode mxnPerkID = new MemberExpressionNode(mxnPerkRegistry,
				new GetExpressionNode(new IdentifierNode(constName, false, 0)), 0);

		ArgumentListNode aln = new ArgumentListNode(null, 0);
		aln.items.clear();
		aln.items.add(ASTUtils.stripMethodFromMXN(root_mxn, new ThisExpressionNode()));
		aln.items.add(new LiteralBooleanNode(false)); // skipActivation is always false in legacy code.
		for (int i = 0; i < args.size(); i++) {
			if (args.get(i) == null) {
				aln.items.add(new LiteralNumberNode("0"));
			} else {
				aln.items.add(args.get(i));
			}
		}

		CallExpressionNode nucen = new CallExpressionNode(new IdentifierNode("applyTo", 0), aln);
		MemberExpressionNode newmxn = new MemberExpressionNode(mxnPerkID, nucen, 0);
		ListNode line = (ListNode) parents.getParentOf(root_mxn);
		line.items.set(0, newmxn);

		ProgramNode program = getProgramNode(line);
		ASTUtils.ensureImportExists(program, "classes.GameData.PerkRegistry");
		return null;
	}

	private ProgramNode getProgramNode(Node curNode) {
		//System.out.println(curNode.getClass().getCanonicalName());
		while(!(curNode instanceof ProgramNode)) {
			curNode = this.parents.getParentOf(curNode);
			if(curNode == null)
				break;
			//System.out.println(curNode.getClass().getCanonicalName());
		}
		return (ProgramNode) curNode;
	}

	@Override
	public void PostProcess() {
		Map<String, Object> perkMap = new HashMap<String, Object>();
		Perk perk;
		for(String ID : unidentifiedPerks) {
			perk = perks.get(ID);
			perkMap.put(perk.ID, perk.serialize());
		}
		File foundPerksFN = new File(FOUND_PERK_FILE_NAME);
		File dataDir = foundPerksFN.getParentFile();
		if (!dataDir.isDirectory()) {
			LOGGER.info("Creating dataDir: "+dataDir.getPath());
			dataDir.mkdirs();
		}
		try {
			FileWriter f = new FileWriter(FOUND_PERK_FILE_NAME);
			DumperOptions _do = new DumperOptions();
			_do.setDefaultFlowStyle(FlowStyle.BLOCK);
			Yaml yaml = new Yaml(_do);
			yaml.dump(perkMap,f);
			f.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
